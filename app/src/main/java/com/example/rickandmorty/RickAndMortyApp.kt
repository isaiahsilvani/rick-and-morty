package com.example.rickandmorty

import android.app.Application
import com.example.rickandmorty.data.di.NetworkModule
import com.example.rickandmorty.data.di.RepositoriesModule
import com.example.rickandmorty.domain.di.DomainModule
import com.example.rickandmorty.presentation.di.PresentationModule
import com.example.rickandmorty.presentation.di.ViewModelFactoryModule
import com.example.rickandmorty.presentation.di.characters.ViewModelModule
import com.example.rickandmorty.presentation.views.characters.characterdetails.CharacterDetailFragment
import com.example.rickandmorty.presentation.views.characters.characterdetails.LocationBottomSheetFragment
import com.example.rickandmorty.presentation.views.characters.characterlist.CharacterListFragment
import com.example.rickandmorty.presentation.views.characters.popups.EpisodesListPopupFragment
import com.example.rickandmorty.presentation.views.characters.popups.OriginPopupFragment
import dagger.Component
import javax.inject.Singleton

// appComponent lives in the Application class to share its lifecycle
class RickAndMortyApp : Application() {
    // Reference to the application graph that is used across the whole app
    val appComponent: appComponent = DaggerappComponent.create()
}

@Singleton
@Component(
    modules = [
        PresentationModule::class,
        NetworkModule::class,
        RepositoriesModule::class,
        DomainModule::class,
        ViewModelFactoryModule::class,
        ViewModelModule::class
    ]
)
interface appComponent {
    // This tells Dagger that CharacterListFra requests injection so the graph needs to
    // satisfy all the dependencies of the fields that LoginActivity is requesting.
    fun inject(mainActivity: MainActivity)
    fun inject(characterListFragment: CharacterListFragment)
    fun inject(episodesListPopupFragment: EpisodesListPopupFragment)
    fun inject(originPopupFragment: OriginPopupFragment)
    fun inject(characterDetailFragment: CharacterDetailFragment)
    fun inject(locationBottomSheetFragment: LocationBottomSheetFragment)
}

/** When using fragments, inject Dagger in the fragment's onAttach() method.
 *  In this case, it can be done before or after calling super.onAttach().
 */

