package com.example.rickandmorty.domain.usecases.characters

import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.rickandmorty.data.paging.RickyMortyPagingSource
import com.example.rickandmorty.data.remote.util.ApiClient
import com.example.rickandmorty.data.repositoriesimpl.CharactersRepositoryImpl
import com.example.rickandmorty.domain.models.CharacterDomain
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetCharactersPagingData @Inject constructor(
    private val charactersRepo: CharactersRepositoryImpl
) {
    operator fun invoke(): Flow<PagingData<CharacterDomain>> {
        return charactersRepo.getCharactersDataStream()
    }
}

