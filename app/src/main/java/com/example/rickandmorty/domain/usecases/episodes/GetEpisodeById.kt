package com.example.rickandmorty.domain.usecases.episodes

import com.example.rickandmorty.data.remote.util.ApiClient
import com.example.rickandmorty.data.repositoriesimpl.EpisodesRepoImpl
import com.example.rickandmorty.domain.models.EpisodeDomain
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject

class GetEpisodeById @Inject constructor(
    private val episodesRepoImpl: EpisodesRepoImpl
) {
    suspend operator fun invoke(id: Int) = callbackFlow {
        episodesRepoImpl.getEpisodeById(id).collectLatest { episode ->
            send(episode?.mapToDomain())
        }
        awaitClose { this.cancel() }
    }
}