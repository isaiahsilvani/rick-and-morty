package com.example.rickandmorty.domain.usecases.locations

import android.util.Log
import com.example.rickandmorty.data.remote.util.ApiClient
import com.example.rickandmorty.data.repositoriesimpl.LocationsRepoImpl
import com.example.rickandmorty.domain.usecases.episodes.parseApiUrlToId
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collectLatest

class GetLocationByUrl(
    private val locationsRepoImpl: LocationsRepoImpl
) {
    suspend operator fun invoke(url: String) = callbackFlow {
        val locationId = parseApiUrlToId(url)
        Log.e("LocationID", locationId.toString())
        locationsRepoImpl.getLocationById(locationId).collectLatest { location ->
            send(location?.mapToDomain())
        }
        awaitClose { this.cancel() }
    }
}