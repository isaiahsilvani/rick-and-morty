package com.example.rickandmorty.domain.usecases.characters

import com.example.rickandmorty.data.remote.util.ApiClient
import com.example.rickandmorty.data.repositoriesimpl.CharactersRepositoryImpl
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject

class GetCharactersByPage @Inject constructor(
    private val charactersRepo: CharactersRepositoryImpl
) {
    suspend operator fun invoke(page: Int) = callbackFlow {
        charactersRepo.getCharacters(page).collectLatest { characters ->
            send(characters?.mapToDomain())
        }
        awaitClose { this.cancel() }
    }
}