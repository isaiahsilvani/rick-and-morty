package com.example.rickandmorty.domain.di

import android.location.Location
import com.example.rickandmorty.data.remote.util.ApiClient
import com.example.rickandmorty.data.repositoriesimpl.CharactersRepositoryImpl
import com.example.rickandmorty.data.repositoriesimpl.EpisodesRepoImpl
import com.example.rickandmorty.data.repositoriesimpl.LocationsRepoImpl
import com.example.rickandmorty.domain.usecases.characters.GetCharacterDetailsById
import com.example.rickandmorty.domain.usecases.characters.GetCharactersByLocation
import com.example.rickandmorty.domain.usecases.characters.GetCharactersByPage
import com.example.rickandmorty.domain.usecases.episodes.GetEpisodeById
import com.example.rickandmorty.domain.usecases.episodes.GetEpisodesByPage
import com.example.rickandmorty.domain.usecases.episodes.GetEpisodesByUrlList
import com.example.rickandmorty.domain.usecases.locations.GetLocationByUrl
import dagger.Module
import dagger.Provides

@Module
object DomainModule {

    @Provides
    fun providesGetLocationByUrl(locationsRepo: LocationsRepoImpl) = GetLocationByUrl(locationsRepo)

    @Provides
    fun providesGetEpisodeByPage(episodesRepo: EpisodesRepoImpl) = GetEpisodesByPage(episodesRepo)

    @Provides
    fun providesGetEpisodeById(episodesRepo: EpisodesRepoImpl) = GetEpisodeById(episodesRepo)

    @Provides
    fun providesGetEpisodesByUrlList(episodesRepo: EpisodesRepoImpl) =
        GetEpisodesByUrlList(episodesRepo)

    @Provides
    fun providesGetCharactersByPage(charactersRepo: CharactersRepositoryImpl) =
        GetCharactersByPage(charactersRepo)

    @Provides
    fun providesGetCharactersByLocation(
        charactersRepo: CharactersRepositoryImpl,
        locationsRepo: LocationsRepoImpl
    ) =
        GetCharactersByLocation(charactersRepo, locationsRepo)

    @Provides
    fun providesGetCharacterDetailsById(charactersRepo: CharactersRepositoryImpl) =
        GetCharacterDetailsById(charactersRepo)
}