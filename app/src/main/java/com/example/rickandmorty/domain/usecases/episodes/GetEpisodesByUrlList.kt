package com.example.rickandmorty.domain.usecases.episodes

import android.util.Log
import com.example.rickandmorty.data.remote.models.Episode
import com.example.rickandmorty.data.remote.util.ApiClient
import com.example.rickandmorty.data.repositoriesimpl.EpisodesRepoImpl
import com.example.rickandmorty.domain.models.EpisodeDomain
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject

class GetEpisodesByUrlList @Inject constructor(
    private val episodesRepoImpl: EpisodesRepoImpl
) {
    suspend operator fun invoke(episodesUrlList: List<String>) = callbackFlow {

        /** Great example of Kotlin flows **/
        val episodeIds = episodesUrlList.map { episodeUrl ->
            parseApiUrlToId(episodeUrl)
        }
        episodesRepoImpl.getAllEpisodes().collectLatest { episodes ->
            withContext(Dispatchers.Default) {
                val episodesWithCharacterInIt = getEpisodesByIds(episodes, episodeIds)
                send(episodesWithCharacterInIt)
            }
        }
        awaitClose { this.cancel() }
    }
}

fun parseApiUrlToId(episodeUrl: String): Int {
    var episodeId = ""
    for (i in episodeUrl.length - 1 downTo 0) {
        val char = episodeUrl[i]
        if (char == '/') {
            break
        }
        episodeId += char
    }
    return episodeId.reversed().toInt()
}

private fun getEpisodesByIds(
    episodes: List<Episode>,
    episodesIdList: List<Int>
): List<EpisodeDomain> {
    val characterEpisodes = mutableListOf<EpisodeDomain>()
    episodesIdList.forEach { id ->
        val episode = episodes.find { episode -> episode.id == id }
        episode?.let { episode ->
            characterEpisodes.add(episode.mapToDomain())
        }
    }
    return characterEpisodes.toList()
}