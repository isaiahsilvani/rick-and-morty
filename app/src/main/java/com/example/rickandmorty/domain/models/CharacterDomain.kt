package com.example.rickandmorty.domain.models

import com.example.rickandmorty.data.remote.models.Location
import com.example.rickandmorty.data.remote.models.Origin

@kotlinx.serialization.Serializable
data class CharacterDomain(
    val created: String,
    val episodes: List<String>,
    val gender: String,
    val id: Int,
    val image: String,
    val location: Location,
    val name: String,
    val origin: Origin,
    val species: String,
    val status: String,
    val type: String,
    val url: String,
    val speciesAndStatus: String = "$status - $species",
    val episodeCount: String,
    val statusColor: Int,
)