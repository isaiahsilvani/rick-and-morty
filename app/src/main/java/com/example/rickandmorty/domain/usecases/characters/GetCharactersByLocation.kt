package com.example.rickandmorty.domain.usecases.characters

import android.location.Location
import android.util.Log
import com.example.rickandmorty.data.remote.util.ApiClient
import com.example.rickandmorty.data.repositoriesimpl.CharactersRepositoryImpl
import com.example.rickandmorty.data.repositoriesimpl.LocationsRepoImpl
import com.example.rickandmorty.domain.models.CharacterDomain
import com.example.rickandmorty.domain.models.LocationDomain
import com.example.rickandmorty.domain.usecases.episodes.parseApiUrlToId
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject

class GetCharactersByLocation @Inject constructor(
    private val charactersRepo: CharactersRepositoryImpl,
    private val locationsRepo: LocationsRepoImpl
) {
    suspend operator fun invoke(locationUrl: String) = callbackFlow {
            val locationId = parseApiUrlToId(locationUrl)
            locationsRepo.getLocationById(locationId).collectLatest { location ->
                val residentIds = location?.residents?.map {
                    parseApiUrlToId(it)
                }
                if (residentIds != null) {
                    charactersRepo.getCharactersByIds(residentIds).collectLatest { character ->
                        if (character != null) {
                            send(
                                character.mapToDomain()
                            )
                        }
                    }
                }
            }
        awaitClose { this.cancel() }
    }
}