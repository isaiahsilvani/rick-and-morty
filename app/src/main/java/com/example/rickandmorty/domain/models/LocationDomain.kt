package com.example.rickandmorty.domain.models

data class LocationDomain(
    val created: String,
    val dimension: String,
    val id: Int,
    val name: String,
    val residents: List<String>,
    val type: String,
    val url: String
)