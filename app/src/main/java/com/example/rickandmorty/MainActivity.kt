package com.example.rickandmorty

import android.os.Bundle
import android.os.PersistableBundle
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI.setupWithNavController
import com.example.rickandmorty.util.BottomNavBarController
import com.google.android.material.bottomnavigation.BottomNavigationView


class MainActivity : FragmentActivity(R.layout.activity_main) {
    override fun onStart() {
        super.onStart()
        setUpBottomNavigation()
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        // Connect this activity, and subsequent fragments, to DI graph by injecting it here
        (applicationContext as RickAndMortyApp).appComponent.inject(this)
        super.onCreate(savedInstanceState, persistentState)
    }

    private fun setUpBottomNavigation() {
        val navController: NavController =
            Navigation.findNavController(this, R.id.navHost)
        val bottomNavigationView =
            findViewById<BottomNavigationView>(R.id.bottom_navigation)
        setupWithNavController(bottomNavigationView, navController)
        // Custom solution to making bottom navigation disappear at certain fragments
        BottomNavBarController.setBottomNavBar(bottomNavigationView)
    }
}

/** When using activities, inject Dagger in the activity's onCreate() method before
 *  calling super.onCreate() to avoid issues with fragment restoration. During the restore phase in super.onCreate(),
 *  an activity attaches fragments that might want to access activity bindings.
 */