package com.example.rickandmorty.data.remote.models

@kotlinx.serialization.Serializable
data class Location(
    val name: String,
    val url: String
)