package com.example.rickandmorty.data.remote.models

import com.example.rickandmorty.domain.models.LocationDomain

data class ResultX(
    val created: String,
    val dimension: String,
    val id: Int,
    val name: String,
    val residents: List<String>,
    val type: String,
    val url: String
) {
    fun mapToDomain() = LocationDomain(
        created, dimension, id, name, residents, type, url
    )
}