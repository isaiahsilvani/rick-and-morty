package com.example.rickandmorty.data.remote.models

data class LocationsResponse(
    val info: InfoX,
    val results: List<Result>
)