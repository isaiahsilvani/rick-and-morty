package com.example.rickandmorty.data.repositoriesimpl.util

import android.util.Log
import com.example.rickandmorty.data.remote.util.SimpleResponse

internal fun <T> handleNetworkRequest(request: SimpleResponse<T>): T? {
    // Checks for out-of-control network issues (no internet, timeout, etc...)
    return if (request.failed) {
        null
        // Checks for network issues in-our-control (authorization, etc...)
    } else if (!request.isSuccessful) {
        null
        // SUCCESS!!
    } else {
        request.body
    }
}