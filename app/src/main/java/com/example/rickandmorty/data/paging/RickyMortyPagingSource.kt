package com.example.rickandmorty.data.paging

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.rickandmorty.data.remote.util.ApiClient
import com.example.rickandmorty.domain.models.CharacterDomain
import javax.inject.Inject

class RickyMortyPagingSource @Inject constructor(
    private val apiClient: ApiClient
) : PagingSource<Int, CharacterDomain>() {

    override fun getRefreshKey(state: PagingState<Int, CharacterDomain>): Int? {
        return null
    }

    override suspend fun load(params: LoadParams<Int>):
            LoadResult<Int, CharacterDomain> {
        return try {
            val currentPage = params.key ?: 1
            val response = apiClient.getCharactersByPage(currentPage)
            val responseData = mutableListOf<CharacterDomain>()
            if (response.isSuccessful) {
                responseData.addAll(response.body.results.map { it.mapToDomain() })
            }
            responseData.forEach {
                Log.e("TEST", it.name)
            }
            LoadResult.Page(
                data = responseData,
                prevKey = if (currentPage == 1) null else -1,
                nextKey = currentPage.plus(1)
            )
        } catch (e: Exception) {
            Log.e("TEST", "${e.message}")
            LoadResult.Error(e)
        }

    }
}