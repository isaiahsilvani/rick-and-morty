package com.example.rickandmorty.data.remote.util

import com.example.rickandmorty.data.remote.models.*
import com.example.rickandmorty.data.remote.services.RickService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class ApiClient @Inject constructor(
    private val rickService: RickService
) {

    suspend fun getLocationById(id: Int): SimpleResponse<ResultX> {
        return safeApiCall {
            withContext(Dispatchers.IO) {
                rickService.getLocationById(id)
            }
        }
    }

    suspend fun getLocationsByPage(page: Int): SimpleResponse<LocationsResponse> {
        return safeApiCall {
            withContext(Dispatchers.IO) {
                rickService.getLocationsByPage(page)
            }
        }
    }

    suspend fun getCharactersByPage(page: Int): SimpleResponse<CharactersResponse> {
        return safeApiCall {
            withContext(Dispatchers.IO) {
                rickService.getCharactersByPage(page)
            }
        }
    }

    suspend fun getCharacterDetailsById(id: Int): SimpleResponse<CharacterResponse> {
        return safeApiCall {
            withContext(Dispatchers.IO) {
                rickService.getCharacterDetailsById(id)
            }
        }
    }

    suspend fun getEpisodesByPage(page: Int): SimpleResponse<EpisodesResponse> {
        return safeApiCall {
            withContext(Dispatchers.IO) {
                rickService.getEpisodesByPage(page)
            }
        }
    }

    suspend fun getEpisodeById(id: Int): SimpleResponse<Episode> {
        return safeApiCall {
            withContext(Dispatchers.IO) {
                rickService.getEpisodeDetailsById(id)
            }
        }
    }

    private inline fun <T> safeApiCall(apiCall: () -> Response<T>): SimpleResponse<T> {
        return try {
            SimpleResponse.success(apiCall.invoke())
        } catch (e: Exception) {
            SimpleResponse.failure(e)
        }
    }

}