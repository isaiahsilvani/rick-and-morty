package com.example.rickandmorty.data.remote.models

data class InfoXX(
    val count: Int,
    val next: String,
    val pages: Int,
    val prev: Any
)