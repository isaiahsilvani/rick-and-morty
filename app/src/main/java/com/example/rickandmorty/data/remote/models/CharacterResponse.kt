package com.example.rickandmorty.data.remote.models

import android.graphics.Color
import com.example.rickandmorty.domain.models.CharacterDomain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

data class CharacterResponse(
    val created: String,
    val episode: List<String>,
    val gender: String,
    val id: Int,
    val image: String,
    val location: Location,
    val name: String,
    val origin: Origin,
    val species: String,
    val status: String,
    val type: String,
    val url: String
) {
    suspend fun mapToDomain() = CharacterDomain(
        created = this.created,
        episodes = this.episode,
        gender = this.gender,
        id = this.id,
        image = this.image,
        location = this.location,
        name = this.name,
        origin = this.origin,
        species = this.species,
        status = this.status,
        type = this.type,
        url = this.url,
        statusColor = withContext(Dispatchers.Default) {
            when (status) {
                "Dead" -> Color.RED
                "Alive" -> Color.GREEN
                else -> Color.GRAY
            }
        },
        episodeCount = if (this.episode.size > 1) {
            "In ${this.episode.size} episodes"
        } else {
            "In 1 episode"
        }
    )

}