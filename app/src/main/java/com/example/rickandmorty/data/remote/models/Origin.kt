package com.example.rickandmorty.data.remote.models

import kotlinx.serialization.Contextual

@kotlinx.serialization.Serializable
data class Origin(
    val name: String,
    val url: String
)