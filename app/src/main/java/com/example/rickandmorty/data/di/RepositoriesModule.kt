package com.example.rickandmorty.data.di

import com.example.rickandmorty.data.remote.util.ApiClient
import com.example.rickandmorty.data.repositoriesimpl.CharactersRepositoryImpl
import com.example.rickandmorty.data.repositoriesimpl.EpisodesRepoImpl
import com.example.rickandmorty.data.repositoriesimpl.LocationsRepoImpl
import dagger.Module
import dagger.Provides

@Module
object RepositoriesModule {

    @Provides
    fun providesCharacterRepo(apiClient: ApiClient) = CharactersRepositoryImpl(apiClient)

    @Provides
    fun providesLocationRepo(apiClient: ApiClient) = LocationsRepoImpl(apiClient)

    @Provides
    fun providesEpisodesRepo(apiClient: ApiClient) = EpisodesRepoImpl(apiClient)

}