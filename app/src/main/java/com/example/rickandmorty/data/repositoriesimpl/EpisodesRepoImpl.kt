package com.example.rickandmorty.data.repositoriesimpl

import com.example.rickandmorty.data.remote.models.Episode
import com.example.rickandmorty.data.remote.util.ApiClient
import com.example.rickandmorty.data.repositoriesimpl.util.handleNetworkRequest
import com.example.rickandmorty.data.remote.models.EpisodesResponse
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import javax.inject.Inject

class EpisodesRepoImpl @Inject constructor(
    private val apiClient: ApiClient
) {

    suspend fun getEpisodesByPage(page: Int): Flow<EpisodesResponse?> = callbackFlow {
        send(
            handleNetworkRequest(
                apiClient.getEpisodesByPage(page)
            )
        )
        awaitClose { this.cancel() }
    }

    suspend fun getAllEpisodes(): Flow<List<Episode>> = callbackFlow {
        val allEpisodes = mutableListOf<Episode>()
        var page = 1
        while (page <= 3) {
            val result = handleNetworkRequest(
                apiClient.getEpisodesByPage(page)
            )?.results
            if (result != null) {
                allEpisodes.addAll(result)
            }
            page++
        }
        send(
            allEpisodes.toList()
        )
        awaitClose { this.cancel() }
    }

    suspend fun getEpisodeById(id: Int): Flow<Episode?> = callbackFlow {
        send(
            handleNetworkRequest(
                apiClient.getEpisodeById(id)
            )
        )
        awaitClose { this.cancel() }
    }

}