package com.example.rickandmorty.data.remote.models

import com.example.rickandmorty.domain.models.EpisodeDomain

data class EpisodesResponse(
    val info: Info,
    val results: List<Episode>
) {
    fun mapToDomain(): List<EpisodeDomain> {
        return results.map { episode ->
            episode.mapToDomain()
        }
    }
}