package com.example.rickandmorty.data.remote.models

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.example.rickandmorty.domain.models.CharacterDomain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

data class CharactersResponse(
    val info: Info,
    val results: List<CharacterResponse>
) {
    suspend fun mapToDomain(): List<CharacterDomain> = this.results.map { character ->
        character.mapToDomain()
    }
}

