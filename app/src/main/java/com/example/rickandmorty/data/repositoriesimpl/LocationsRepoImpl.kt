package com.example.rickandmorty.data.repositoriesimpl

import com.example.rickandmorty.data.remote.models.*
import com.example.rickandmorty.data.remote.util.ApiClient
import com.example.rickandmorty.data.remote.util.SimpleResponse
import com.example.rickandmorty.data.repositoriesimpl.util.handleNetworkRequest
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.ProducerScope
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import javax.inject.Inject

class LocationsRepoImpl @Inject constructor(
    private val apiClient: ApiClient
) {

    suspend fun getLocationsByPage(page: Int): Flow<LocationsResponse?> = callbackFlow {
        send(
            handleNetworkRequest(
                apiClient.getLocationsByPage(page)
            )
        )
        awaitClose { this.cancel() }
    }

    suspend fun getLocationById(id: Int): Flow<ResultX?> = callbackFlow {
        send(
            handleNetworkRequest(
                apiClient.getLocationById(id)
            )
        )
        awaitClose { this.cancel() }
    }

    suspend fun getAllLocations(): Flow<LocationsResponse?> = callbackFlow {
        var counter = 1
        while (counter <= 7) {
            send(
                handleNetworkRequest(
                    apiClient.getLocationsByPage(counter)
                )
            )
            counter++
        }
        awaitClose { this.cancel() }
    }
}