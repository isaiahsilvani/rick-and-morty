package com.example.rickandmorty.data.repositoriesimpl

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.rickandmorty.data.paging.RickyMortyPagingSource
import com.example.rickandmorty.data.remote.util.ApiClient
import com.example.rickandmorty.data.repositoriesimpl.util.handleNetworkRequest
import com.example.rickandmorty.data.remote.models.CharactersResponse
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import com.example.rickandmorty.data.remote.models.CharacterResponse
import com.example.rickandmorty.domain.models.CharacterDomain
import javax.inject.Inject

class CharactersRepositoryImpl @Inject constructor(
    private val apiClient: ApiClient
) {

    suspend fun getCharacters(page: Int = 1): Flow<CharactersResponse?> = callbackFlow {
        send(
            handleNetworkRequest(
                apiClient.getCharactersByPage(page)
            )
        )
        awaitClose { this.cancel() }
    }

    suspend fun getCharacterDetailsById(id: Int): Flow<CharacterResponse?> = callbackFlow {
        send(
            handleNetworkRequest(
                apiClient.getCharacterDetailsById(id)
            )
        )
        awaitClose { this.cancel() }
    }

    suspend fun getCharactersByIds(characterIds: List<Int>): Flow<CharacterResponse?> = callbackFlow {
        characterIds.forEach { id ->
            send(
                handleNetworkRequest(
                    apiClient.getCharacterDetailsById(id)
                )
            )
        }
        awaitClose { this.cancel() }
    }

    fun getCharactersDataStream(): Flow<PagingData<CharacterDomain>> {
        return Pager(PagingConfig(pageSize = 20)) {
            RickyMortyPagingSource(apiClient)
        }.flow
    }

}