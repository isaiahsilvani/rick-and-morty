package com.example.rickandmorty.data.remote.models

import com.example.rickandmorty.domain.models.EpisodeDomain

data class Episode(
    val air_date: String,
    val characters: List<String>,
    val created: String,
    val episode: String,
    val id: Int,
    val name: String,
    val url: String
) {
    fun mapToDomain() = EpisodeDomain(
        air_date = this.air_date,
        characters = this.characters,
        created = this.created,
        episode = this.episode,
        id = this.id,
        name = this.name,
        url = this.url
    )
}