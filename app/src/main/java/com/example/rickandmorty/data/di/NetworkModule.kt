package com.example.rickandmorty.data.di

import com.example.rickandmorty.data.remote.services.RickService
import com.example.rickandmorty.data.remote.util.ApiClient
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

@Module
object NetworkModule {
    // NETWORK STUFF
    @Provides
    fun providesApiClient(rickService: RickService): ApiClient = ApiClient(rickService)

    @Provides
    fun providesRickService(retrofitBuilder: Retrofit.Builder): RickService =
        retrofitBuilder.baseUrl(RickService.BASE_URL).build().create()

    @Provides
    fun providesRetrofitBuilder(): Retrofit.Builder =
        Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
}