package com.example.rickandmorty.data.remote.util

import com.example.rickandmorty.data.remote.services.RickService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Inject

class RetrofitProvider @Inject constructor(
    private val retrofitBuilder: Retrofit.Builder
) {
    fun getRickService(): RickService {
        return retrofitBuilder.baseUrl(RickService.BASE_URL).build().create()
    }
}