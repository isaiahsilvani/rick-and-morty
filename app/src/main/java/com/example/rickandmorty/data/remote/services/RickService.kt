package com.example.rickandmorty.data.remote.services

import com.example.rickandmorty.data.remote.models.*
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RickService {

    companion object {
        const val BASE_URL = "https://rickandmortyapi.com/api/"
        private const val CHARACTERS_ENDPOINT = "character"
        private const val CHARACTER_ENDPOINT = "character/{id}"
        private const val LOCATIONS_ENDPOINT = "location"
        private const val LOCATION_ENDPOINT = "location/{id}"
        private const val EPISODES_ENDPOINT = "episode"
        private const val EPISODE_ENDPOINT = "episode/{id}"
    }

    @GET(CHARACTERS_ENDPOINT)
    suspend fun getCharactersByPage(@Query("page") page: Int): Response<CharactersResponse>

    @GET(CHARACTER_ENDPOINT)
    suspend fun getCharacterDetailsById(@Path("id") id: Int): Response<CharacterResponse>

    @GET(EPISODES_ENDPOINT)
    suspend fun getEpisodesByPage(@Query("page") page: Int): Response<EpisodesResponse>

    @GET(EPISODE_ENDPOINT)
    suspend fun getEpisodeDetailsById(@Path("id") id: Int): Response<Episode>

    @GET(LOCATIONS_ENDPOINT)
    suspend fun getLocationsByPage(@Query("page") page: Int): Response<LocationsResponse>

    @GET(LOCATION_ENDPOINT)
    suspend fun getLocationById(@Path("id") id: Int): Response<ResultX>

}