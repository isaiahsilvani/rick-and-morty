package com.example.rickandmorty.util

import androidx.core.view.isVisible
import com.google.android.material.bottomnavigation.BottomNavigationView

// This object will control when the Bottom Navigation Bar is visible to the user
object BottomNavBarController {

    private var bottomNavBar: BottomNavigationView? = null

    // Set the instance so that we can control it
    fun setBottomNavBar(bottomNavBar: BottomNavigationView) {
        BottomNavBarController.bottomNavBar = bottomNavBar
    }
    // Set the visibility of the bottom navigation bar
    fun isVisible(boolean: Boolean) {
        if (boolean != bottomNavBar?.isVisible && bottomNavBar != null) {
            bottomNavBar?.let { navBar ->
                navBar.apply { isVisible = boolean }
            }
        }
    }
}