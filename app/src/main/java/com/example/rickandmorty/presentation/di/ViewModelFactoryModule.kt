package com.example.rickandmorty.presentation.di

import androidx.lifecycle.ViewModelProvider
import com.example.rickandmorty.presentation.viewmodels.ViewModelProviderFactory
import dagger.Binds
import dagger.Module


@Module
abstract class ViewModelFactoryModule {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelProviderFactory?): ViewModelProvider.Factory?
}