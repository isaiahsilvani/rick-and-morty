package com.example.rickandmorty.presentation.views.characters.popups

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.rickandmorty.RickAndMortyApp
import com.example.rickandmorty.databinding.FragmentCharacterOriginPopupBinding
import com.example.rickandmorty.domain.models.CharacterDomain
import com.example.rickandmorty.domain.models.LocationDomain
import com.example.rickandmorty.presentation.viewmodels.CharactersSharedViewModel
import com.example.rickandmorty.presentation.viewmodels.util.SharedCharacterListVMHelper
import com.example.rickandmorty.presentation.views.util.safelyCollectLatest

class OriginPopupFragment : DialogFragment() {

    private lateinit var binding: FragmentCharacterOriginPopupBinding
    private lateinit var sharedViewModel: CharactersSharedViewModel
    private lateinit var residentsAdapter: OriginsResidentsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        sharedViewModel = SharedCharacterListVMHelper.getSharedInstance()!!
        binding = FragmentCharacterOriginPopupBinding.inflate(layoutInflater)
        initRecyclerView()
        initObservers()
        initViews()
        return binding.root
    }

    private fun initRecyclerView() {
        residentsAdapter = OriginsResidentsAdapter()
        binding.rvResidents.adapter = residentsAdapter
        binding.rvResidents.layoutManager = GridLayoutManager(requireContext(), 2)
    }

    private fun initObservers() {
        safelyCollectLatest(sharedViewModel.state) { state ->
            setOriginLocationUi(
                state.isKnownOrigin,
                state.selectedCharacter,
                state.selectedLocation
            )
            binding.progressBar.isVisible = state.isLoadingResidents && state.residents.isEmpty()
            Log.e("STATE!!!", state.residents.toString())
            residentsAdapter.addResidents(state.residents)
        }
    }

    private fun initViews() {
        with(binding) {
            btnClose.setOnClickListener {
                dismiss()
            }
        }
    }

    /** Decides what is displayed to the user when they click a character's
    origin location button depending on if origin is known or unknown */
    private fun setOriginLocationUi(
        knownOrigin: Boolean?,
        character: CharacterDomain?,
        location: LocationDomain?
    ) {
        if (knownOrigin == false) {
            displayOriginUnknown()
        } else {
            displayOriginDetails(location, character)
        }
    }

    private fun displayOriginUnknown() {
        val originUnknown = "Origin Unknown..."
        with(binding) {
            tvLocationName.isVisible = false
            tvDimension.isVisible = false
            tvHeader.isVisible = false
            tvResidentsHeader.isVisible = false
            textView.text = originUnknown
        }
    }

    private fun displayOriginDetails(
        location: LocationDomain?,
        character: CharacterDomain?
    ) {
        val headerTxt =
            if (character != null) "${character.name}'s origin" else "character's origin"
        val dimensionTxt = "${location?.dimension}\n\nType - ${location?.type}"
        val residentsHeader = "Residents of this location"
        with(binding) {
            textView.isVisible = false
            tvHeader.text = headerTxt
            tvResidentsHeader.text = residentsHeader
            tvDimension.text = dimensionTxt
            tvLocationName.text = location?.name
            tvResidentsHeader.isVisible = true
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        // We must manually clear data fields in the ViewModel since this is a Shared View Model,
        // so it won't be cleared when we navigate away from this fragment
        sharedViewModel.clearCharacterOrigin()
    }

    override fun onAttach(context: Context) {
        (context.applicationContext as RickAndMortyApp).appComponent.inject(this)
        super.onAttach(context)
    }
}