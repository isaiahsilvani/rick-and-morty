package com.example.rickandmorty.presentation.views.characters.characterdetails

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.example.rickandmorty.RickAndMortyApp
import com.example.rickandmorty.databinding.FragmentCharacterDetailsBinding
import com.example.rickandmorty.domain.models.CharacterDomain
import com.example.rickandmorty.presentation.viewmodels.CharacterSharedDetailsViewModel
import com.example.rickandmorty.presentation.viewmodels.ViewModelProviderFactory
import com.example.rickandmorty.presentation.views.characters.popups.EpisodesListPopupFragmentDirections
import com.example.rickandmorty.presentation.views.characters.popups.EpisodesPopupAdapter
import com.example.rickandmorty.presentation.views.util.safelyCollectLatest
import com.example.rickandmorty.util.BottomNavBarController
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import javax.inject.Inject

class CharacterDetailFragment : Fragment() {
    // Define what we'll need for the fragment here
    private lateinit var binding: FragmentCharacterDetailsBinding
    private lateinit var character: CharacterDomain
    private lateinit var episodesAdapter: EpisodesPopupAdapter
    lateinit var viewModel: CharacterSharedDetailsViewModel
    private val args by navArgs<CharacterDetailFragmentArgs>()
    // Widely recognized solution for injecting ViewModels with Dependencies using Dagger 2
    @Inject
    lateinit var providerFactory: ViewModelProviderFactory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        character = Json.decodeFromString(args.characterSerialized)
        viewModel =
            ViewModelProvider(this, providerFactory)[CharacterSharedDetailsViewModel::class.java]
        // Since we're getting data from NAV ARGS, we need to pass it to viewModel immediately
        viewModel.setUpDetailFragment(character)
        binding = FragmentCharacterDetailsBinding.inflate(layoutInflater)
        initRecyclerView()
        initViews()
        initObservers()
        BottomNavBarController.isVisible(false)
        return binding.root
    }

    private fun initRecyclerView() {
        episodesAdapter = EpisodesPopupAdapter(this::navigateToEpisodeDetail)
        binding.rvEpisodes.adapter = episodesAdapter
        binding.rvEpisodes.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun initObservers() {
        safelyCollectLatest(viewModel.state) { state ->
            binding.progressEpisodeList.isVisible = state.isLoading
            episodesAdapter.addEpisodes(state.episodes)
        }
    }

    private fun initViews() {
        with(binding) {
            val speciesAndGender = "${character.species} - ${character.gender}"
            val episodesWithCharacter = "Episodes with ${character.name}"
            tvCharacterName.text = character.name
            tvSpeciesAndGender.text = speciesAndGender
            tvStatus.text = character.status
            imgCharacter.load(character.image)
            tvOriginName.text = character.origin.name
            tvCurrentLocationName.text = character.location.name
            tvEpisodesWithCharacter.text = episodesWithCharacter
            setLocationAndOriginBindings(
                listOf(tvCurrentLocationName, tvOriginName)
            )
        }
    }

    private fun setLocationAndOriginBindings(locations: List<TextView>) {
        locations.forEach { location ->
            location.setOnClickListener {
                if (it == binding.tvCurrentLocationName) viewModel.setLocationStateValueToTrue()
                if (it == binding.tvOriginName) viewModel.setOriginStateValueToTrue()
                if (location.text != "unknown") {
                    LocationBottomSheetFragment().show(childFragmentManager, "LocationPopup")
                }
            }
        }
    }

    override fun onAttach(context: Context) {
        // Inject Dagger component here...
        (context.applicationContext as RickAndMortyApp).appComponent.inject(this)
        super.onAttach(context)
    }

    private fun navigateToEpisodeDetail(serializedEpisode: String) {
        val action =
            CharacterDetailFragmentDirections.actionCharacterDetailFragmentToEpisodeDetailFragment(
                serializedEpisode
            )
        findNavController().navigate(action)
    }

}