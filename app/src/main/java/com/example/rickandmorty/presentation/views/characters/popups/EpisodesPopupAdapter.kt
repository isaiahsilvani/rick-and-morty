package com.example.rickandmorty.presentation.views.characters.popups

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.rickandmorty.databinding.EpisodeListItemBinding
import com.example.rickandmorty.domain.models.EpisodeDomain
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class EpisodesPopupAdapter(
    private val navigateToEpisodeDetail: (String) -> Unit
) : RecyclerView.Adapter<EpisodesPopupAdapter.EpisodeViewHolder>() {

    private val episodeList = mutableListOf<EpisodeDomain>()

    inner class EpisodeViewHolder(binding: EpisodeListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val episodeName = binding.tvEpisodeName
        val episodeCharacterCount = binding.tvCharacterCount
        val airDate = binding.tvAirDate
        val episodeNumber = binding.tvEpisodeNumber
        val episodeView = binding.viewEpisode
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeViewHolder {
        return EpisodeViewHolder(
            EpisodeListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: EpisodeViewHolder, position: Int) {
        with(holder) {
            val episode = episodeList[position]
            val characterCountStr = "This episode featured ${episode.characters.size} characters."
            val airDateStr = "Aired on ${episode.air_date}"
            val episodeNumberStr = "Episode # ${episode.id}"
            episodeName.text = episode.name
            episodeCharacterCount.text = characterCountStr
            airDate.text = airDateStr
            episodeNumber.text = episodeNumberStr
            episodeView.setOnClickListener {
                navigateToEpisodeDetail(Json.encodeToString(episode))
            }
        }
    }

    override fun getItemCount() = episodeList.size

    fun addEpisodes(episodes: List<EpisodeDomain>) {
        val oldSize = episodes.size
        episodeList.addAll(episodes)
        notifyItemRangeChanged(oldSize, episodeList.size)
    }
}