package com.example.rickandmorty.presentation.views.characters.characterdetails

import com.example.rickandmorty.domain.models.CharacterDomain
import com.example.rickandmorty.domain.models.EpisodeDomain
import com.example.rickandmorty.domain.models.LocationDomain

data class CharacterDetailState(
    val episodes: List<EpisodeDomain> = emptyList(),
    var selectedCharacter: CharacterDomain? = null,
    var selectedOriginDetails: Boolean = false,
    var selectedLocationDetails: Boolean = false,
    var currentLocation: LocationDomain? = null,
    var isLoadingResidents: Boolean = false,
    var residents: List<CharacterDomain> = emptyList(),
    val isLoading: Boolean = false
)
