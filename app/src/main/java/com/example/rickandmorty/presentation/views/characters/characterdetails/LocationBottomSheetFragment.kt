package com.example.rickandmorty.presentation.views.characters.characterdetails

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rickandmorty.RickAndMortyApp
import com.example.rickandmorty.databinding.FragmentLocationBottomSheetBinding
import com.example.rickandmorty.presentation.viewmodels.CharacterSharedDetailsViewModel
import com.example.rickandmorty.presentation.viewmodels.util.SharedCharacterDetailsVMHelper
import com.example.rickandmorty.presentation.views.characters.popups.OriginsResidentsAdapter
import com.example.rickandmorty.presentation.views.util.safelyCollectLatest
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import javax.inject.Inject

class LocationBottomSheetFragment : BottomSheetDialogFragment() {
    lateinit var binding: FragmentLocationBottomSheetBinding
    lateinit var sharedViewModel: CharacterSharedDetailsViewModel
    lateinit var residentsAdapter: OriginsResidentsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Custom solution to using a Shared View Model
        sharedViewModel = SharedCharacterDetailsVMHelper.getSharedInstance()!!
        binding = FragmentLocationBottomSheetBinding.inflate(layoutInflater)
        sharedViewModel.getLocationDetails()
        initRecyclerView()
        initObservers()
        initViews()
        return binding.root
    }

    private fun initRecyclerView() {
        residentsAdapter = OriginsResidentsAdapter()
        binding.rvResidents.adapter = residentsAdapter
        binding.rvResidents.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
    }

    private fun initObservers() {
        safelyCollectLatest(sharedViewModel.state) { state ->
            state.currentLocation?.let { location ->
                with(binding) {
                    tvLocationName.text = location.name
                    tvDimension.text = location.dimension
                    tvType.text = location.type
                }
            }
            residentsAdapter.addResidents(state.residents)
        }
    }

    private fun initViews() {
        with(binding) {
            btnClose.setOnClickListener { dismiss() }
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        // Since the SharedViewModel will NOT be cleared when this Dialog Fragment dismisses,
        // we must manually clear the state fields that applies to this specific Fragment
        sharedViewModel.resetSelectedLocation()
        sharedViewModel.setOriginAndLocationStateValuesToFalse()
        super.onDismiss(dialog)
    }

    override fun onAttach(context: Context) {
        (context.applicationContext as RickAndMortyApp).appComponent.inject(this)
        super.onAttach(context)
    }

}