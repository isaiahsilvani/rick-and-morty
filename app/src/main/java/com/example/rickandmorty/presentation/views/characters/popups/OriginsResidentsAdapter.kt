package com.example.rickandmorty.presentation.views.characters.popups

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.rickandmorty.databinding.ResidentsPopupListItemBinding
import com.example.rickandmorty.domain.models.CharacterDomain

class OriginsResidentsAdapter : RecyclerView.Adapter<OriginsResidentsAdapter.ResidentViewHolder>() {

    private val residentList = mutableListOf<CharacterDomain>()

    inner class ResidentViewHolder(binding: ResidentsPopupListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val characterName = binding.tvCharacterName
        val characterImg = binding.imgCharacterHolder
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResidentViewHolder {
        return ResidentViewHolder(
            ResidentsPopupListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ResidentViewHolder, position: Int) {
        with(holder) {
            val character = residentList[position]
            characterName.text = character.name
            characterImg.load(character.image)
        }
    }

    override fun getItemCount() = residentList.size

    fun addResidents(residents: List<CharacterDomain>) {
        val oldSize = residentList.size
        residentList.clear()
        residentList.addAll(residents)
        notifyItemRangeChanged(oldSize, residentList.size)
    }
}