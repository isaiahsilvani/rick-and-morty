package com.example.rickandmorty.presentation.views.util

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.example.rickandmorty.R
import com.example.rickandmorty.databinding.AssetLoadStateFooterViewItemBinding

// This class creates the Load State UI for our PagerAdapter
class AssetLoadStateViewHolder(
    private val binding: AssetLoadStateFooterViewItemBinding,
    retry: () -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.retryButton.setOnClickListener { retry.invoke() }
    }

    fun bind(loadState: LoadState) {
        if (loadState is LoadState.Error) {
            binding.errorMsg.text = loadState.error.localizedMessage
        }
        binding.progressBar.isVisible = loadState is LoadState.Loading
        binding.retryButton.isVisible = loadState is LoadState.Error
        binding.errorMsg.isVisible = loadState is LoadState.Error
    }

    companion object {
        fun create(parent: ViewGroup, retry: () -> Unit): AssetLoadStateViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.asset_load_state_footer_view_item, parent, false)
            val binding = AssetLoadStateFooterViewItemBinding.bind(view)
            return AssetLoadStateViewHolder(binding, retry)
        }
    }
}