package com.example.rickandmorty.presentation.views.util

import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter

class AssetLoadStateAdapter(private val retry: () -> Unit) : LoadStateAdapter<AssetLoadStateViewHolder>() {
    override fun onBindViewHolder(holder: AssetLoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): AssetLoadStateViewHolder {
        return AssetLoadStateViewHolder.create(parent, retry)
    }
}