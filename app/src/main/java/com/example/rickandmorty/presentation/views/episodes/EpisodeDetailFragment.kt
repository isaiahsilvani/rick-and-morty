package com.example.rickandmorty.presentation.views.episodes

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.rickandmorty.databinding.FragmentEpisodeDetailBinding
import com.example.rickandmorty.domain.models.EpisodeDomain
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

class EpisodeDetailFragment : Fragment() {

    lateinit var binding: FragmentEpisodeDetailBinding
    lateinit var episode: EpisodeDomain

    private val args by navArgs<EpisodeDetailFragmentArgs>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEpisodeDetailBinding.inflate(layoutInflater)
        episode = Json.decodeFromString(args.serializedEpisode)
        initViews()
        return binding.root
    }

    private fun initViews() {
        with (binding) {
            val airDate = "Aired on: ${episode.air_date}"
            val createdDate = "Created on: ${episode.created}"
            tvEpisodeName.text = episode.name
            tvCreatedOn.text = createdDate
            tvAiredOn.text = airDate
            for (character in episode.characters) {
                Log.e("PRINT", character)
            }
        }
    }
}