package com.example.rickandmorty.presentation.views.episodes

import androidx.fragment.app.Fragment
import com.example.rickandmorty.R

class EpisodeListFragment : Fragment(R.layout.fragment_episodelist)