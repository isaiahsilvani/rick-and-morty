package com.example.rickandmorty.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.example.rickandmorty.data.remote.util.ApiClient
import com.example.rickandmorty.domain.models.CharacterDomain
import com.example.rickandmorty.domain.usecases.characters.GetCharactersByLocation
import com.example.rickandmorty.domain.usecases.characters.GetCharactersByPage
import com.example.rickandmorty.domain.usecases.characters.GetCharactersPagingData
import com.example.rickandmorty.domain.usecases.episodes.GetEpisodesByUrlList
import com.example.rickandmorty.domain.usecases.locations.GetLocationByUrl
import com.example.rickandmorty.presentation.views.characters.characterlist.CharacterListState
import com.example.rickandmorty.presentation.viewmodels.util.SharedCharacterListVMHelper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

class CharactersSharedViewModel @Inject constructor(
    private val getEpisodesByUrlList: GetEpisodesByUrlList,
    private val getLocationByUrl: GetLocationByUrl,
    private val getCharactersByLocation: GetCharactersByLocation,
    getCharactersPagingData: GetCharactersPagingData
) : ViewModel() {

    private val _state = MutableStateFlow(CharacterListState())
    val state = _state.asStateFlow()

    val charactersData = getCharactersPagingData().cachedIn(viewModelScope)

    init {
        SharedCharacterListVMHelper.setSharedViewModel(this)
    }

    override fun onCleared() {
        super.onCleared()
        // Since the SharedCharacterVMHelper Singleton Object outlives the ViewModel,
        // we must manually clear the instance of it from the object.
        SharedCharacterListVMHelper.clearSharedViewModel()
    }

    fun getEpisodesByCharacter(character: CharacterDomain) {
        viewModelScope.launch {
            _state.value =
                _state.value.copy(selectedCharacter = character, isLoadingPopup = true)
            getEpisodesByUrlList.invoke(character.episodes).collectLatest { episode ->
                _state.value = _state.value.copy(
                    episodesByCharacter = episode,
                    isLoadingPopup = false
                )
            }
        }
    }

    fun clearCharacterEpisodes() {
        _state.value =
            _state.value.copy(episodesByCharacter = emptyList(), selectedCharacter = null)
    }

    fun clearCharacterOrigin() {
        _state.value = _state.value.copy(
            selectedLocation = null,
            selectedCharacter = null,
            residents = emptyList()
        )
    }

    fun getLocationByCharacter(character: CharacterDomain) {
        viewModelScope.launch {
            val origin = character.origin.url
            if (origin == "") {
                _state.value = _state.value.copy(isKnownOrigin = false)
            } else {
                _state.value = _state.value.copy(
                    isKnownOrigin = true,
                    selectedCharacter = character,
                    isLoadingPopup = true
                )
                getLocationByUrl.invoke(character.origin.url).collectLatest { location ->
                    _state.value = _state.value.copy(
                        selectedLocation = location,
                        isLoadingPopup = false
                    )
                }
            }
        }
    }

    fun getResidentsByOrigin(character: CharacterDomain) {
        viewModelScope.launch {
            if (character.origin.url != "") {
                _state.value = _state.value.copy(isLoadingResidents = true)
                getCharactersByLocation(character.origin.url).collect { character ->
                    val currentResidents = _state.value.residents.toMutableList()
                    currentResidents.add(character)
                    _state.value = _state.value.copy(residents = currentResidents)
                }
            } else {
                _state.value = _state.value.copy(isLoadingResidents = false)
            }
        }
    }

}