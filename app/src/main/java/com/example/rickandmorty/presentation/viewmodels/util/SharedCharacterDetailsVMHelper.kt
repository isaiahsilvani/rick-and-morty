package com.example.rickandmorty.presentation.viewmodels.util

import com.example.rickandmorty.presentation.viewmodels.CharacterSharedDetailsViewModel

object SharedCharacterDetailsVMHelper {

    private var sharedCharacterDetailViewModel: CharacterSharedDetailsViewModel? = null

    fun setSharedCharacterDetailsViewModel(characterSharedDetailsViewModel: CharacterSharedDetailsViewModel) {
        sharedCharacterDetailViewModel = characterSharedDetailsViewModel
    }

    fun clearSharedViewModel() {
        sharedCharacterDetailViewModel = null
    }

    fun getSharedInstance() = sharedCharacterDetailViewModel

}