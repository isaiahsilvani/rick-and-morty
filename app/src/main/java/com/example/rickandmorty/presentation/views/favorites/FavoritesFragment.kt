package com.example.rickandmorty.presentation.views.favorites

import androidx.fragment.app.Fragment
import com.example.rickandmorty.R

class FavoritesFragment : Fragment(R.layout.fragment_favorites)