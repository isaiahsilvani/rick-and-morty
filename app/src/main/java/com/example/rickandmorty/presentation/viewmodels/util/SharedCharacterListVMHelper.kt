package com.example.rickandmorty.presentation.viewmodels.util

import com.example.rickandmorty.presentation.viewmodels.CharactersSharedViewModel

object SharedCharacterListVMHelper {

    var sharedCharacterViewModel: CharactersSharedViewModel? = null

    fun setSharedViewModel(sharedCharacterViewModel: CharactersSharedViewModel) {
        SharedCharacterListVMHelper.sharedCharacterViewModel = sharedCharacterViewModel
    }

    fun clearSharedViewModel() {
        sharedCharacterViewModel = null
    }

    fun getSharedInstance() = sharedCharacterViewModel
}