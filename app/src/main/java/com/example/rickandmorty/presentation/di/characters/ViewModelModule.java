package com.example.rickandmorty.presentation.di.characters;

import androidx.lifecycle.ViewModel;

import com.example.rickandmorty.presentation.di.ViewModelKey;
import com.example.rickandmorty.presentation.viewmodels.CharacterSharedDetailsViewModel;
import com.example.rickandmorty.presentation.viewmodels.CharactersSharedViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(CharactersSharedViewModel.class)
    public abstract ViewModel bindCharactersSharedViewModel(CharactersSharedViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(CharacterSharedDetailsViewModel.class)
    public abstract ViewModel bindCharactersSharedDetailsViewModel(CharacterSharedDetailsViewModel viewModel);
}
