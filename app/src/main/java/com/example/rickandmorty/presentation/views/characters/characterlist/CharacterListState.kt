package com.example.rickandmorty.presentation.views.characters.characterlist

import com.example.rickandmorty.domain.models.CharacterDomain
import com.example.rickandmorty.domain.models.EpisodeDomain
import com.example.rickandmorty.domain.models.LocationDomain

data class CharacterListState(
    val isLoadingScreen: Boolean = false,
    val isLoadingPopup: Boolean = false,
    val isLoadingResidents: Boolean = false,
    val isKnownOrigin: Boolean = false,
    val showErrorPopup: Boolean = false,
    val selectedCharacter: CharacterDomain? = null,
    val selectedLocation: LocationDomain? = null,
    val residents: List<CharacterDomain> = emptyList(),
    val characters: List<CharacterDomain> = emptyList(),
    val episodesByCharacter: List<EpisodeDomain> = emptyList(),
    val locationsByCharacter: List<LocationDomain> = emptyList(),
)