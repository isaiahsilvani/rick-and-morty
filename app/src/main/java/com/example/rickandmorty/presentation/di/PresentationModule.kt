package com.example.rickandmorty.presentation.di

import com.example.rickandmorty.domain.usecases.characters.GetCharactersByLocation
import com.example.rickandmorty.domain.usecases.characters.GetCharactersByPage
import com.example.rickandmorty.domain.usecases.episodes.GetEpisodesByUrlList
import com.example.rickandmorty.domain.usecases.locations.GetLocationByUrl
import com.example.rickandmorty.presentation.viewmodels.CharacterSharedDetailsViewModel
import com.example.rickandmorty.presentation.viewmodels.CharactersSharedViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object PresentationModule {

}