package com.example.rickandmorty.presentation.viewmodels

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.example.rickandmorty.domain.models.CharacterDomain
import com.example.rickandmorty.domain.usecases.characters.GetCharactersByLocation
import com.example.rickandmorty.domain.usecases.characters.GetCharactersPagingData
import com.example.rickandmorty.domain.usecases.episodes.GetEpisodesByUrlList
import com.example.rickandmorty.domain.usecases.locations.GetLocationByUrl
import com.example.rickandmorty.presentation.viewmodels.util.SharedCharacterDetailsVMHelper
import com.example.rickandmorty.presentation.views.characters.characterdetails.CharacterDetailState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

class CharacterSharedDetailsViewModel @Inject constructor(
    private val getEpisodesByUrlList: GetEpisodesByUrlList,
    private val getLocationByUrl: GetLocationByUrl,
    private val getCharactersByLocation: GetCharactersByLocation,
    private val getCharactersPagingData: GetCharactersPagingData
) : ViewModel() {

    private val _state = MutableStateFlow(CharacterDetailState())
    val state = _state.asStateFlow()

    private fun getEpisodes(episodes: List<String>) {
        viewModelScope.launch {
            _state.value = _state.value.copy(isLoading = true, episodes = emptyList())
            getEpisodesByUrlList.invoke(episodes).collectLatest { episodes ->
                _state.value = _state.value.copy(episodes = episodes, isLoading = false)
            }
        }
    }

    fun setUpDetailFragment(character: CharacterDomain) {
        viewModelScope.launch {
            SharedCharacterDetailsVMHelper.setSharedCharacterDetailsViewModel(this@CharacterSharedDetailsViewModel)
            _state.value = _state.value.copy(selectedCharacter = character)
            getEpisodes(character.episodes)
        }
    }

    fun getLocationDetails() {
        viewModelScope.launch {
            _state.value.selectedCharacter?.let { character ->
                if (_state.value.selectedLocationDetails) {
                    getLocationDetailsByUrl(character.location.url)
                } else if (_state.value.selectedOriginDetails) {
                    getLocationDetailsByUrl(character.origin.url)
                }
            }
        }
    }

    private fun getLocationDetailsByUrl(locationUrl: String) {
        viewModelScope.launch {
            /** GET SUM DATA MANE!! **/
            getLocationByUrl.invoke(locationUrl).collectLatest { location ->
                location?.let {
                    _state.value = _state.value.copy(currentLocation = location)
                    getResidentsByLocationUrl(it.url)
                }
            }
        }
    }

    fun setLocationStateValueToTrue() {
        viewModelScope.launch {
            _state.value = _state.value.copy(selectedLocationDetails = true)
        }
    }

    fun setOriginStateValueToTrue() {
        viewModelScope.launch {
            _state.value = _state.value.copy(selectedOriginDetails = true)
        }
    }

    fun setOriginAndLocationStateValuesToFalse() {
        viewModelScope.launch {
            _state.value =
                _state.value.copy(selectedOriginDetails = false, selectedLocationDetails = false)
        }
    }

    fun resetSelectedLocation() {
        viewModelScope.launch {
            _state.value = _state.value.copy(currentLocation = null)
        }
    }

    private fun getResidentsByLocationUrl(locationUrl: String) {
        viewModelScope.launch {
            if (locationUrl != "") {
                _state.value = _state.value.copy(isLoadingResidents = true)
                getCharactersByLocation(locationUrl).collect { character ->
                    val currentResidents = _state.value.residents.toMutableList()
                    currentResidents.add(character)
                    _state.value =
                        _state.value.copy(residents = currentResidents, isLoading = false)
                }
            } else {
                _state.value = _state.value.copy(isLoadingResidents = false)
            }
        }
    }

}