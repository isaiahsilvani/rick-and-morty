package com.example.rickandmorty.presentation.views.characters.characterlist

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.rickandmorty.databinding.CharacterListItemBinding
import com.example.rickandmorty.domain.models.CharacterDomain
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class CharactersPagingAdapter(
    private val getEpisodesByCharacter: (character: CharacterDomain) -> Unit,
    private val displayEpisodesListPopup: () -> Unit,
    private val displayOriginLocationPopup: () -> Unit,
    private val getLocationByCharacter: (character: CharacterDomain) -> Unit,
    private val getResidentsByOrigin: (character: CharacterDomain) -> Unit,
    private val navigateToCharacterDetails: (characterSerialized: String) -> Unit
) : PagingDataAdapter<CharacterDomain, CharactersPagingAdapter.CharacterViewHolder>(diffCallback) {

    inner class CharacterViewHolder(binding: CharacterListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val characterName: TextView = binding.tvCharacterName
        val characterImage: ImageView = binding.imgCharacter
        val statusAndSpecies: TextView = binding.tvStatusAndSpecies
        val statusCircle: View = binding.vStatusCircle
        val episodeCount: TextView = binding.tvEpisodeCount
        val btnViewEpisodes: ImageView = binding.btnEpisodeList
        val btnViewDetails: ImageView = binding.btnDetails
        val btnViewLocations: ImageView = binding.btnLocations
        val btnFavorite: ImageView = binding.btnFavorite
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        return CharacterViewHolder(
            CharacterListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        val currentChar = getItem(position)
        with(holder) {
            currentChar?.let { character ->
                characterName.text = character.name
                statusAndSpecies.text = character.speciesAndStatus
                episodeCount.text = character.episodeCount
                setCharacterButtons(
                    btnViewEpisodes, btnViewLocations, btnFavorite, btnViewDetails, character
                )
                characterImage.load(character.image)
                statusCircle.backgroundTintList = ColorStateList.valueOf(character.statusColor)
            }
        }
    }

    // TODO Does this break SRP? Is it worth it?
    private fun setCharacterButtons(
        episodesBtn: View,
        locationsBtn: View,
        favoritesBtn: View,
        detailsBtn: View,
        character: CharacterDomain
    ) {
        episodesBtn.setOnClickListener {
            getEpisodesByCharacter(character)
            displayEpisodesListPopup()
        }
        locationsBtn.setOnClickListener {
            displayOriginLocationPopup()
            getLocationByCharacter(character)
            getResidentsByOrigin(character)
        }
        detailsBtn.setOnClickListener {
            /** SERIALIZES A CHARACTER TO A JSON STRING **/
            navigateToCharacterDetails(Json.encodeToString(character))
        }
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<CharacterDomain>() {
            override fun areItemsTheSame(
                oldItem: CharacterDomain,
                newItem: CharacterDomain
            ): Boolean =
                oldItem.name == newItem.name

            override fun areContentsTheSame(
                oldItem: CharacterDomain,
                newItem: CharacterDomain
            ): Boolean =
                oldItem == newItem
        }
    }
}