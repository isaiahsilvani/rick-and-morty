package com.example.rickandmorty.presentation.views.characters.popups

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rickandmorty.RickAndMortyApp
import com.example.rickandmorty.databinding.FragmentPopupEpisodesBinding
import com.example.rickandmorty.presentation.viewmodels.CharactersSharedViewModel
import com.example.rickandmorty.presentation.viewmodels.util.SharedCharacterListVMHelper
import com.example.rickandmorty.presentation.views.util.safelyCollectLatest

class EpisodesListPopupFragment : DialogFragment() {

    private lateinit var binding: FragmentPopupEpisodesBinding
    private lateinit var sharedViewModel: CharactersSharedViewModel
    private lateinit var episodesPopupAdapter: EpisodesPopupAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        sharedViewModel = SharedCharacterListVMHelper.getSharedInstance()!!
        binding = FragmentPopupEpisodesBinding.inflate(layoutInflater)
        initRecyclerView()
        initViews()
        initObservers()
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        // This code makes the dialog fragment almost the same size as the screen
        dialog?.window
            ?.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
            )
    }

    private fun initObservers() {
        safelyCollectLatest(sharedViewModel.state) { state ->
            with(binding) {
                val headerStr = "Episodes with ${state.selectedCharacter?.name ?: "???"}"
                tvHeader.text = headerStr
                // Loading indicator
                progressBar.isVisible = state.isLoadingPopup
            }
            state.episodesByCharacter.let { episodes ->
                episodesPopupAdapter.addEpisodes(episodes)
            }
        }
    }

    private fun initViews() {
        binding.btnClose.setOnClickListener { dismiss() }
    }

    private fun initRecyclerView() {
        episodesPopupAdapter = EpisodesPopupAdapter(this::navigateToEpisodeDetail)
        binding.rvEpisodes.adapter = episodesPopupAdapter
        binding.rvEpisodes.layoutManager = LinearLayoutManager(requireContext())
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        sharedViewModel.clearCharacterEpisodes()
    }

    override fun onAttach(context: Context) {
        (context.applicationContext as RickAndMortyApp).appComponent.inject(this)
        super.onAttach(context)
    }

    private fun navigateToEpisodeDetail(serializedEpisode: String) {
        val action =
            EpisodesListPopupFragmentDirections.actionEpisodesListPopupFragmentToEpisodeDetailFragment(
                serializedEpisode
            )
        findNavController().navigate(action)
    }
}