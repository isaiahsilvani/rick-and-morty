package com.example.rickandmorty.presentation.views.characters.characterlist

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rickandmorty.R
import com.example.rickandmorty.RickAndMortyApp
import com.example.rickandmorty.databinding.FragmentCharacterlistBinding
import com.example.rickandmorty.presentation.viewmodels.CharactersSharedViewModel
import com.example.rickandmorty.presentation.viewmodels.ViewModelProviderFactory
import com.example.rickandmorty.presentation.views.characters.popups.EpisodesListPopupFragment
import com.example.rickandmorty.presentation.views.characters.popups.OriginPopupFragment
import com.example.rickandmorty.presentation.views.util.AssetLoadStateAdapter
import com.example.rickandmorty.presentation.views.util.safelyCollect
import com.example.rickandmorty.util.BottomNavBarController
import com.example.rickandmorty.presentation.views.util.safelyCollectLatest
import javax.inject.Inject

class CharacterListFragment : Fragment(R.layout.fragment_characterlist) {

    private lateinit var binding: FragmentCharacterlistBinding
    private lateinit var sharedViewModel: CharactersSharedViewModel
    private lateinit var charactersAdapter: CharactersPagingAdapter

    @Inject
    lateinit var providerFactory: ViewModelProviderFactory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCharacterlistBinding.inflate(layoutInflater)
        initRecyclerView()
        initObservers()
        BottomNavBarController.isVisible(true)
        return binding.root
    }

    override fun onAttach(context: Context) {
        (context.applicationContext as RickAndMortyApp).appComponent.inject(this)
        sharedViewModel =
            ViewModelProvider(this, providerFactory)[CharactersSharedViewModel::class.java]
        super.onAttach(context)
    }

    private fun initRecyclerView() {
        charactersAdapter = CharactersPagingAdapter(
            sharedViewModel::getEpisodesByCharacter,
            this::displayEpisodesListPop,
            this::displayOriginLocationPopup,
            sharedViewModel::getLocationByCharacter,
            sharedViewModel::getResidentsByOrigin,
            this::navigateToCharacterDetails
        )
        binding.rvCharacters.layoutManager = LinearLayoutManager(requireContext())
        binding.rvCharacters.adapter = charactersAdapter.withLoadStateHeaderAndFooter(
            header = AssetLoadStateAdapter { charactersAdapter.retry() },
            footer = AssetLoadStateAdapter { charactersAdapter.retry() }
        )
    }

    private fun initObservers() {
        with (binding) {
            // Observe data coming from View Model to submit to PagingAdapter
            safelyCollectLatest(sharedViewModel.charactersData) { data ->
                charactersAdapter.submitData(data)
            }
            // Observe the load state of Pager3 library
            safelyCollect(charactersAdapter.loadStateFlow) { loadState ->
                val isListEmpty =
                    loadState.refresh is LoadState.NotLoading && charactersAdapter.itemCount == 0
                // show empty list
                rvCharacters.isVisible = isListEmpty
                // Only show the list if refresh succeeds.
                rvCharacters.isVisible = !isListEmpty
                // Show progress bar if load state is refreshing or loading
                progressBar.isVisible = loadState.source.refresh is LoadState.Loading
                // Show the retry state if initial load or refresh fails.
                retryButton.isVisible = loadState.source.refresh is LoadState.Error
                // Toast on any error, regardless of whether it came from RemoteMediator or PagingSource
                val errorState = loadState.source.append as? LoadState.Error
                    ?: loadState.source.prepend as? LoadState.Error
                    ?: loadState.append as? LoadState.Error
                    ?: loadState.prepend as? LoadState.Error
                errorState?.let {
                    Toast.makeText(
                        requireContext(),
                        "\uD83D\uDE28 Wooops ${it.error}",
                        Toast.LENGTH_LONG
                    ).show()
            }
        }
    }
}

private fun displayEpisodesListPop() {
    EpisodesListPopupFragment()
        .show(childFragmentManager, "Episodes Popup")
}

private fun displayOriginLocationPopup() {
    OriginPopupFragment()
        .show(childFragmentManager, "Origin Popup")
}

private fun navigateToCharacterDetails(characterSerialized: String) {
    val options = NavOptions.Builder()
        .setLaunchSingleTop(true)
        .build()
    val action =
        CharacterListFragmentDirections.actionCharactersfragmentToCharacterDetailFragment(
            characterSerialized
        )
    findNavController().navigate(action, options)
}
}